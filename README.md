###Prerequisites
* Java 8
* Gradle

###Framework
* This application is built on top of the excellent [Dropwizard framework](http://dropwizard.codahale.com/).
* Dependency injection was achieved with [Google Guice](https://code.google.com/p/google-guice/).

###IDE Integration
* `gradle eclipse` - To generate Eclipse project structure.
* `gradle idea` - To generate Intellij IDEA project structure.

###Lifecycle

* `gradle clean` - Cleans the build directory and JARs.
* `gradle assemble` - Compiles and creates JAR, doesn't run tests.
* `gradle test` - Runs tests.
* `gradle build` - Compiles, tests and creates JAR.
* `gradle jar` - Creates a JAR with all dependencies included.
* `gradle run` - Starts the server on the default port 34343, logs to stdout.
* `java -jar <jar_location> server Doitfast.yml` - Run standalone.

###Steps to setup on a desktop
* git clone git@bitbucket.org:rohit_yo/doitfast.git
* cd doitfast
* ./gradlew clean build jar
* java -jar build/libs/doitfast-1.0.jar server Doitfast.yml


##API
###Lowest price for all the books of the author
* HttpMethod: POST
* HttpURL: http://localhost:34343/author/books
* RequestBody: {"goodreadsLink":<Goodreads author link >}
* Example request body:{"goodreadsLink":"https://www.goodreads.com/author/show/3367145.Chris_Guillebeau"}