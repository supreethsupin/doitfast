package com.asap.doitfast;

import com.asap.doitfast.core.configurations.ExternalKeysConfiguration;
import com.asap.doitfast.core.configurations.ClientConfiguration;
import com.asap.doitfast.core.configurations.URLConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by supreeth.nag on 14/04/16.
 */
@Getter
@Setter
public class DoitfastConfiguration extends Configuration{

    @JsonProperty
    @NotNull
    @NotEmpty
    private String name;

    @JsonProperty
    private int aggregatorPoolSize = 32;

    @Valid
    @NotNull
    @JsonProperty
    ExternalKeysConfiguration externalKeysConfiguration;

    @Valid
    @NotNull
    @JsonProperty
    private URLConfiguration urlConfiguration;

    @Valid
    @NotNull
    @JsonProperty
    private ClientConfiguration clientConfiguration= new ClientConfiguration();

}
