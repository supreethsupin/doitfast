package com.asap.doitfast;

import com.asap.doitfast.core.configurations.ExternalKeysConfiguration;
import com.asap.doitfast.core.configurations.URLConfiguration;
import com.asap.doitfast.services.XMLParsingService;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.sun.jersey.api.client.Client;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.setup.Environment;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.concurrent.*;

/**
 * Created by supreeth.nag on 14/04/16.
 */
public class DoitfastModule extends AbstractModule {

    @Override
    protected void configure() {
    }


    @Provides
    @Singleton
    public Client provideJerseyClient(DoitfastConfiguration configuration, Environment environment) {
        Client client = new JerseyClientBuilder(environment).using(configuration.getClientConfiguration().getJerseyClient()).build(configuration.getName());
        client.setReadTimeout(configuration.getClientConfiguration().getReadTimeout());
        client.setConnectTimeout(configuration.getClientConfiguration().getConnectionTimeout());
        return client;
    }

    @Provides
    public DocumentBuilder provideDocumentBuilder() throws ParserConfigurationException {
        return DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    @Provides
    @Singleton
    public XMLParsingService provideXMLParsinService(DocumentBuilder documentBuilder) throws ParserConfigurationException {
        return new XMLParsingService(documentBuilder);
    }

    @Provides
    public URLConfiguration getURLConfig(DoitfastConfiguration configuration)
    {
        return configuration.getUrlConfiguration();
    }

    @Provides
    public ExternalKeysConfiguration getAwsConfiguration(DoitfastConfiguration configuration){
        return configuration.getExternalKeysConfiguration();
    }

    @Provides
    @Singleton
    public ExecutorService provideExecutorService(DoitfastConfiguration configuration) {
        final ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("product-aggregator")
                .build();
        return new ThreadPoolExecutor(configuration.getAggregatorPoolSize(), configuration.getAggregatorPoolSize(), 3000,
                TimeUnit.SECONDS, new LinkedBlockingQueue<>(), threadFactory);
    }
}
