package com.asap.doitfast;

import com.google.inject.Stage;
import com.hubspot.dropwizard.guice.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Created by supreeth.nag on 14/04/16.
 */
public class DoitfastService extends Application<DoitfastConfiguration> {
    public static void main(String... args) throws Exception {
        new DoitfastService().run(args);
    }
    @Override
    public void initialize(Bootstrap<DoitfastConfiguration> bootstrap) {
        GuiceBundle<DoitfastConfiguration> guiceBundle = GuiceBundle.<DoitfastConfiguration>newBuilder().
                addModule(new DoitfastModule()).
                setConfigClass(DoitfastConfiguration.class).
                enableAutoConfig(getClass().getPackage().getName()).
                build(Stage.DEVELOPMENT);
        bootstrap.addBundle(guiceBundle);

    }

    @Override
    public void run(DoitfastConfiguration configuration, Environment environment) throws Exception {

    }

    @Override
    public String getName() {
        return "doitfast";
    }
}
