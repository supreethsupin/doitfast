package com.asap.doitfast.callables;

import com.asap.doitfast.models.Book;
import com.asap.doitfast.services.AmazonProductService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.concurrent.Callable;


/**
 * Created by supreeth.nag on 05/05/16.
 */
@AllArgsConstructor
public class AmazonLookUpTaskForMinPrice implements Callable<List<Book>> {
    AmazonProductService amazonProductService;
    List<Book> books;

    @Override
    public List<Book> call() throws Exception {

        books.stream().forEach(book -> {
            try {
                Float price=amazonProductService.callProductLookupForMinPrice(amazonProductService.lookupURLBuilder(book));
                book.setPrice(price/100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return books;
    }
}
