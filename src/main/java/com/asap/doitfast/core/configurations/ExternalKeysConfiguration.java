package com.asap.doitfast.core.configurations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by supreeth.nag on 04/05/16.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class ExternalKeysConfiguration {
    String awsAccessKeyId;
    String associateTag;
    String secretKey;
    String goodReadsDeveloperKey;
}
