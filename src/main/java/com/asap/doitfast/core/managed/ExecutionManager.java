package com.asap.doitfast.core.managed;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;

/**
 * Created by supreeth.nag on 05/05/16.
 */
@Singleton
@Getter
public class ExecutionManager implements Managed {

    private static final Logger logger = LoggerFactory.getLogger(ExecutionManager.class);
    private final ExecutorService instance;

    @Inject
    public ExecutionManager(ExecutorService executorService) {
        this.instance = executorService;
    }

    @Override
    public void start() throws Exception {
        logger.info("Binding and Starting Execution Manager...");
    }

    @Override
    public void stop() throws Exception {
        logger.info("Shutting down executor services");
        instance.shutdown();
        logger.info("Executor Service shutdown complete");
    }
}
