package com.asap.doitfast.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.xml.annotate.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created by supreeth.nag on 04/05/16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class Author {

    @JacksonXmlProperty(localName = "link")
    @JsonProperty
    String goodreadsLink;
    @JsonProperty
    String id;
    @JacksonXmlProperty(localName = "name")
    @JsonProperty
    String name;

    @JacksonXmlProperty(localName = "books")
    @JsonProperty
    List<Book> books;
}
