package com.asap.doitfast.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.xml.annotate.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Created by supreeth.nag on 04/05/16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class Book {
    @JsonProperty
    @JacksonXmlProperty(localName = "isbn13")
    String isbn13;
    @JsonProperty
    @JacksonXmlProperty(localName = "title")
    String title;
    @JsonProperty
    Float price;
    public Book(String isbn13,String title){
        this.isbn13=isbn13;
        this.title=title;
    }
    public Book(String isbn13){
        this.isbn13=isbn13;
    }
}
