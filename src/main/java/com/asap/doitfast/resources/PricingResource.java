package com.asap.doitfast.resources;

import com.asap.doitfast.models.Author;
import com.asap.doitfast.models.Book;
import com.asap.doitfast.services.AmazonProductService;
import com.asap.doitfast.services.GoodreadsService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


/**
 * Created by supreeth.nag on 14/04/16.
 */
@Slf4j
@Path("/author")
@Singleton
public class PricingResource {

    private final GoodreadsService goodreadsService;
    private final AmazonProductService amazonProductService;

    @Inject
    public PricingResource(GoodreadsService goodreadsService, AmazonProductService amazonProductService) {
        this.goodreadsService = goodreadsService;
        this.amazonProductService = amazonProductService;
    }
//
//    @GET
//    public String test(){
//        return "Working";
//    }

    @POST
    @Path("/books")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Book> getBestPrice(Author author) throws Exception{
        author.setBooks(goodreadsService.getBooksForAuthor(author));
        log.info("look up request for author "+author.getId());
        return amazonProductService.getPriceForBooks(author.getBooks());

    }
}
