package com.asap.doitfast.services;

import com.asap.doitfast.core.configurations.ExternalKeysConfiguration;
import com.asap.doitfast.core.configurations.URLConfiguration;
import com.asap.doitfast.core.managed.ExecutionManager;
import com.asap.doitfast.models.Book;
import com.google.inject.Inject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by supreeth.nag on 04/05/16.
 */@Slf4j
public class AmazonProductService {

    private final Client client;
    private final URLConfiguration urlConfiguration;
    private final XMLParsingService xmlParsingService;
    private final ExternalKeysConfiguration externalKeysConfiguration;
    private final ExecutionManager executionManager;

    @Inject
    public AmazonProductService(Client client, URLConfiguration urlConfiguration, XMLParsingService xmlParsingService, ExternalKeysConfiguration externalKeysConfiguration, ExecutionManager executionManager) {
        this.client = client;
        this.urlConfiguration = urlConfiguration;
        this.xmlParsingService = xmlParsingService;
        this.externalKeysConfiguration = externalKeysConfiguration;
        this.executionManager = executionManager;
    }


    public String lookupURLBuilder(Book book) throws URISyntaxException, MalformedURLException, SignatureException, NoSuchAlgorithmException, UnsupportedEncodingException {
        ArrayList<BasicNameValuePair> queryParams = new ArrayList<>();
        queryParams.add(new BasicNameValuePair("AWSAccessKeyId", externalKeysConfiguration.getAwsAccessKeyId()));
        queryParams.add(new BasicNameValuePair("AssociateTag", externalKeysConfiguration.getAssociateTag()));
        queryParams.add(new BasicNameValuePair("IdType", "ISBN"));
        queryParams.add(new BasicNameValuePair("ItemId", book.getIsbn13()));
        queryParams.add(new BasicNameValuePair("MerchantId", "All"));
        queryParams.add(new BasicNameValuePair("Operation", "ItemLookup"));
        queryParams.add(new BasicNameValuePair("ResponseGroup", "OfferFull"));
        queryParams.add(new BasicNameValuePair("SearchIndex", "Books"));
        queryParams.add(new BasicNameValuePair("Service", "AWSECommerceService"));
        queryParams.add(new BasicNameValuePair("Timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(new Date())));

        String signature = getSignature("GET", urlConfiguration.getAmazonProductServiceUrl().substring(7), "/onca/xml", queryParams);
        queryParams.add(new BasicNameValuePair("Signature", signature));
        StringBuilder buffer = new StringBuilder();
        queryParams.stream().forEach(pair -> buffer.append(pair.toString() + "&"));
        buffer.deleteCharAt(buffer.length() - 1);
        return urlConfiguration.getAmazonProductServiceUrl() + "/onca/xml?" + buffer.toString();

    }

    public List<Book> getPriceForBooks(List<Book> books) throws NoSuchAlgorithmException, MalformedURLException, SignatureException, UnsupportedEncodingException, URISyntaxException, ExecutionException, InterruptedException {

//        List<Future> futures = new ArrayList();
//        Lists.partition(books, 10).stream().forEach(chunkOfBooks -> {
//
//            futures.add(executionManager.getInstance().submit(new AmazonLookUpTaskForMinPrice(this,chunkOfBooks)));
//        });
//        for(Future future : futures)
//            future.get();


        books.stream().forEach(book -> {
            try {
                String lookUpUrl = lookupURLBuilder(book);
                book.setPrice(new Float( callProductLookupForMinPrice(lookUpUrl)/ 100));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return books;
    }

    public synchronized Float callProductLookupForMinPrice(String url) {
        WebResource resource = client.resource(url);
        try {
            String lookupResult = resource.accept("application/xml").get(String.class);
            return xmlParsingService.getMinPriceForBook(lookupResult);
        } catch (Exception e) {
            e.printStackTrace();
            return new Float(0);
        }
    }

    public String getSignature(String HTTPRequestMethod, String host, String path, List<BasicNameValuePair> query_params) throws NoSuchAlgorithmException, UnsupportedEncodingException, MalformedURLException, URISyntaxException, SignatureException {

        String canonicalURL = HTTPRequestMethod + "\n" + host + "\n" + path + "\n" + URLEncodedUtils.format(query_params, "utf-8");
//        log.info(canonicalURL);
        String signature = calculateRFC2104HMAC(canonicalURL, externalKeysConfiguration.getSecretKey()).replace("=", "%3D").replace("+", "%2B");
//        log.info(signature);
        return signature;
    }

    public static String calculateRFC2104HMAC(String data, String key)
            throws java.security.SignatureException {
        String HMAC_SHA256_ALGORITHM = "HmacSHA256";
        String result;
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA256_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            result = Base64.encode(rawHmac);
        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
        return result;
    }
}

