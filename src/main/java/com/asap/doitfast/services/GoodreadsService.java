package com.asap.doitfast.services;

import com.asap.doitfast.core.configurations.ExternalKeysConfiguration;
import com.asap.doitfast.core.configurations.URLConfiguration;
import com.asap.doitfast.models.Author;
import com.asap.doitfast.models.Book;
import com.google.inject.Inject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

/**
 * Created by supreeth.nag on 04/05/16.
 */

public class GoodreadsService {
    private final URLConfiguration urlConfiguration;
    private final Client client;
    private final XMLParsingService xmlParsingService;
    ExternalKeysConfiguration externalKeysConfiguration;

    @Inject
    public GoodreadsService(URLConfiguration urlConfiguration, XMLParsingService xmlParsingService, Client client,ExternalKeysConfiguration externalKeysConfiguration) {
        this.urlConfiguration = urlConfiguration;
        this.xmlParsingService = xmlParsingService;
        this.client = client;
        this.externalKeysConfiguration = externalKeysConfiguration;
    }


    public List<Book> getBooksForAuthor(Author author) throws IOException, ParserConfigurationException, SAXException {
        String tokens[]= author.getGoodreadsLink().split("/");
        author.setId(tokens[tokens.length-1]);
        WebResource webResource = client.resource(urlConfiguration.getGoodreadsUrl()+ "?key="+externalKeysConfiguration.getGoodReadsDeveloperKey()+"&id="+author.getId()+"&per_page=1000");
        String result= webResource.accept("application/xml").get(String.class);
        return xmlParsingService.getBooksForAuthor(result);

    }
}
