package com.asap.doitfast.services;

import com.asap.doitfast.models.Book;
import com.google.inject.Inject;
import org.w3c.dom.CharacterData;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by supreeth.nag on 04/05/16.
 */
public class XMLParsingService {
        private final DocumentBuilder documentbuilder;

    @Inject
    public XMLParsingService(DocumentBuilder documentbuilder) {
        this.documentbuilder = documentbuilder;
    }

    List<Book> getBooksForAuthor(String xmlRecords) throws IOException, SAXException, ParserConfigurationException {

        /*JSONObject xmlJSONObj= XML.toJSONObject(xmlRecords);
        System.out.println(xmlJSONObj);


        JacksonXmlModule module = new JacksonXmlModule();
        GoodreadsResponse goodreadsResponse = new GoodreadsResponse();
        ObjectMapper mapper = new XmlMapper();
        try {
            goodreadsResponse = mapper.readValue(xmlRecords, GoodreadsResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.asList(new Book[1]);

//        return goodreadsResponse.getAuthor().getBooks();*/

        Document doc =  documentbuilder.parse(new ByteArrayInputStream(xmlRecords.getBytes()));
        NodeList nodes =  doc.getElementsByTagName("book");
        ArrayList<Book> books=new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);
            NodeList isbnList = element.getElementsByTagName("isbn13");
            Element isbn = (Element) isbnList.item(0);
            String ISBN=getStringForElement(isbn);

            if (!ISBN.equals(""))
            {
                NodeList titleList = element.getElementsByTagName("title");
                Element title = (Element) titleList.item(0);
                books.add(new Book(getStringForElement(isbn),getStringForElement(title)));
            }
        }

        return books;
    }

    public  String getStringForElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    public String getCharacterDataFromElement(Element e) {

        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    public float getMinPriceForBook(String lookupResult) throws IOException, SAXException {
        Document doc = documentbuilder.parse(new ByteArrayInputStream(lookupResult.getBytes()));
        NodeList offerSummary = doc.getElementsByTagName("OfferSummary");
        if(offerSummary.getLength()==0)
            return new Float(0);
        Element element= (Element) offerSummary.item(0);
        NodeList price_nodes=element.getElementsByTagName("Amount");
        int i;
        float min = (float) 10000;
        for(i=0;i<price_nodes.getLength();i++)
        {
            Element priceElement= (Element) price_nodes.item(i);
            float bookPrice = Float.valueOf(getCharacterDataFromElement(priceElement));
            if(bookPrice<min)
                min=bookPrice;
        }
        return min;
    }
}
